# Mod Categories for Categorized Favorites Menu

## Introduction

This mod extends the [Categorized Favorites Menu](https://www.nexusmods.com/skyrimspecialedition/mods/16416) for many mods, mainly Spells.

What makes this list special is, that it adds subcategories to all schools of spells.
This list was heavily inspired by [Custom Favorites Menu config for Mages](https://www.nexusmods.com/skyrimspecialedition/mods/35626) from 1ceSpark.

## Disclaimer

These categories are subjective and not perfect.
Feel free to manipulate the list as you wish.
It's easy, really.

## Included Mods

### Spell Mods:

- [Vanilla Skyrim](https://elderscrolls.fandom.com/wiki/Spells_(Skyrim))
- [Ace Blood Magic](https://www.nexusmods.com/skyrimspecialedition/mods/16995)
- [Andromeda - Unique Standing Stones of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/14910)
- [Apocalypse - Magic of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/1090)
- [Arcanum - A New Age of Magic](https://www.nexusmods.com/skyrimspecialedition/mods/23488)
- [ankai - Senbonzakura Kageyoshi SE](https://www.nexusmods.com/skyrimspecialedition/mods/30802)
- [Dark Falling Grave SE](https://www.nexusmods.com/skyrimspecialedition/mods/26630)
- [Elemental Destruction Magic Redux](https://www.nexusmods.com/skyrimspecialedition/mods/37211)
- [Eo Geom Summon Swords Special Edition](https://www.nexusmods.com/skyrimspecialedition/mods/10397)
- [Glamoril - Magic of Time and Space](https://www.nexusmods.com/skyrimspecialedition/mods/13089)
- [Imperious - Races of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/1315)
- [Mysticism - A Magic Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/27839)
- [Odin - Skyrim Magic Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/46000)
- [Ordinator - Perks of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/1137)
- [Path of the Anti-Mage](https://www.nexusmods.com/skyrimspecialedition/mods/19668)
- [Phenderix Magic World/Evolved](https://www.nexusmods.com/skyrimspecialedition/mods/6551)
- [Psijic Teleport Spells - SSE](https://www.nexusmods.com/skyrimspecialedition/mods/5643)
- [Sacrosanct - Vampires of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/3928)
- [Shadow Spell Package](https://www.nexusmods.com/skyrimspecialedition/mods/13299)
- [Triumvirate - Mage Archetypes](https://www.nexusmods.com/skyrimspecialedition/mods/39170)
- [Unlimited Blade Works Special Edition](https://www.nexusmods.com/skyrimspecialedition/mods/9930)
- [Wintersun - Faiths of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/22506)

### Weapon:

- Vanilla Skyrim
- General words like "Blade, ..."

### Armory:

- Vanilla Skyrim
- General words like "Shield"

### Potions:

- Vanilla Skyrim
- General Potion names

## Spell Categories

All spells of the included mods belong to one of the following categories.
I startes with many subcaegories and merged them together over time to make the categories compatible with all includes mods.
Some spell schools use the categories introduced by [Arcanum - A New Age of Magic](https://www.nexusmods.com/skyrimspecialedition/mods/23488).

### Alteration

- **Manipulate - Self**: Manipulates the Caster [Oakflesh]
- **Manipulate - Other**: Manupulates other living beings, mostly enemies [Paralyze, Damage Weapon, Acceleration Rune]
- **Manipulate - Environment**: Manipulates the non-living environment [Telekinesis, Open Novice Lock,Raise Wal]

### Conjuration

Here we use the categories from [Arcanum - A New Age of Magic](https://www.nexusmods.com/skyrimspecialedition/mods/23488).

- **Atromancy**: Spells related to summoning creatures
- **Necromancy**: Spells related to manipulate the undead
- **Soulbind**: Bound weapons, soul traps and soul related magic
- **Utility/Buffs**: Contains the few spells that do either not contain to any of the other categories or multiple. Like Buffing undead AND summpns.

### Destruction

Here I had a choice to make. There were two options to categorize them.
Element type (fire, ice) or casting type (concentrate, immediate, rune).
I decided for the first variant. It makes categorizing a lot easier and there would be too much overlap when using the second variant.

- **Fire**
- **Ice**
- **Shock**
- **Earth**
- **Water**
- **Wind**
- **Non Elemental**
- **Multi Element**
- **Poison**
- **Shadow**
- **Blood**
- **Vampire**
- **Hemomancy**: Only used for Sacrosanct

### Illusion

This uses some categories from [Arcanum - A New Age of Magic](https://www.nexusmods.com/skyrimspecialedition/mods/23488) and extends them.

- **Psychosis**: Manipulates the psyche of others [Calm, Fear]
- **Manipulation and Mindplay**: Makes others do what you want and manipulates them in other ways [Mind Vision, Enslave the Week, Command, charm]
- **Illusory Manifestation**: Magic that makes illusions to become reality. Can directly harm the enemy. [Summon an illusion to attack the enemy, Ceate and fire an arrow]
- **Secrecy**: Everything stealth related [Cameleon, Invisibility, Muffle]
- **Mentalism**: Effects that negate the enemies magic and uses it aganist hin (see [Arcanum - A New Age of Magic](https://www.nexusmods.com/skyrimspecialedition/mods/23488) for more details)
- **Trickery**: Other utility based magic [Clairvoyance, Vision of the Tenth Eye, Deep Analysis, Dispel Magic]

### Restoration

This category was hardest to do.

At first I had distinct categories for **Self** and **Others**. However there are too many spells that heal or strengthen both, the caster and others.
Also there was too much clutter when adding **Self and Other**.

There are some spells that do both, heal and empower.
In that case heal always heal wins and I put them intothe heal category.

Damaging spells are all combined in one category.
This was also done to avoid clutter and satisfy overlap.

- **Healing**: Spells that heal the caster or others
- **Protection and empower**: Strengthens or protects the caster or others [Ward, Mass Immortality]
- **Dacay**: Everything that harms living beings. [Poison, Disease, Pain, Cripple, Death, Decay]
- **False Healing**: Only for Arcanum - Spells that first heal the target, then takes away life after some time
- **Purification**: Sun spells and everything that damages the undead [Sun Fire, Dust to Dust, Righteous Authority]


## Powers and Shouts

This probably needs more work.
As we use mods we might want to make 2 or 3 main categories for this.
Powers - Shouts

- **Vampire**: Everything Vampire related
- **Anti Mage**: Only for [Path of the Anti-Mage](https://www.nexusmods.com/skyrimspecialedition/mods/19668)
- **Blood**: Only for [Ace Blood Magic](https://www.nexusmods.com/skyrimspecialedition/mods/16995)
- **Sword Magic**: For everything with anime-like flying swords. Mods from [elysees](https://www.nexusmods.com/skyrimspecialedition/users/1034867?tab=user+files)
- **Standing Stones**: Standing Stone powers, mainly [Andromeda - Unique Standing Stones of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/14910)
- **Other**: Other non-categorial powers. Contains the ones from [Imperious - Races of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/1315) and [Ordinator - Perks of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/1137)

# How to use this

The file favoritesmenu.cfg goes into Data->Interface.
You may zip this and import it into any mod manager.
You need [Categorized Favorites Menu](https://www.nexusmods.com/skyrimspecialedition/mods/16416).

The favoritesmenu.cfg provided here includes spells for [Mysticism - A Magic Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/27839) and [Odin - Skyrim Magic Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/46000).
There is a minor conflict as [Mysticism - A Magic Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/27839) makes Paralysis an Illusion spell instead of an Alteration spell.
If you encounter that conflict or any missmatch, you can easily correct that error.

The files in the "Categories" folder are working files.
Each file contains the subcategory.
Each subcategory contains the mods included and their spells

Example:

```
# Manipulation - Self,
excludes = Shadow Dancer,
--ACE BLOOD MAGIC--,
Alter Blood,Blood Pump,Graft,Husk,Numbing Ritual,
--APOCALYPSE--,
Longstride,Drop Zone,Ocato's Recital,Reynos' Fins,Strength of Earth,Spell Twine,Knowledge is Power,Wind Running,Prepare for Adventure,
--DARK ENVOY..,
Bat Form,Dark Aura,Shadow Armor,
```

Explanation:
the first line is the subcategory.
The second line contains strings to exclude from the list. That line has to be taken over to the sublist completely. See the example below.
The third line contains the mod name.
The fourth line contains the Spell list.
The fifth line contains the next mod.
...


It is easy to build the keywords for the config file from that list.
Simply delete the new line from each line except the "excludes" line

The result will be
```
[sublist]
name = Manipulation - Self:
title_color = #d4ac0d 
parent_list = Alteration
enabled = true
keywords = --ACE BLOOD MAGIC--,Alter Blood,Blood Pump,Graft,Husk,Numbing Ritual,--APOCALYPSE--,Longstride,Drop Zone,Ocato's Recital,Reynos' Fins,Strength of Earth,Spell Twine,Knowledge is Power,Wind Running,Prepare for Adventure,--DARK ENVOY..,Bat Form,Dark Aura,Shadow Armor,--MYSTICISM--,Feather,Windwalker,Buoyancy,Fire Shell,Frost Shell,Shock Shell,Poison Shell,Slowfall,Sea Stride,Pack Mule,Windrunner,Flame Shield,Ice Shield,Lightning Shield,Venom Shield,Mark,Recall,Shalidor’s Beacon,--ODIN--,Feet of Notorgo,Alarm,Reynos' Fins,Ease Burden,Slowfall,Alter Body,Resist Elements,Ocato's Spell Trigger,Orc Strength,Leaguestep,Milestones,--PATH OF THE ANTI-MAGE--,Dark Mending,--PHENDERIX MAGIC EVOLVED--,Teleport -,--SHADOW SPELL Package--,Life Tap,Shadow Enchantments,Shadowform,Nighteye,--TRIUMVIRATE--,Force of Nature,Wildshape,Chase the Horizon,Shadow Stride,Shadow Dance,Stave of Ferocity,Stave of Binding,Sacred Hearth,Eye of the All-Maker,--VANILLA SKYRIM--,Equilibrium,Oakflesh,Stoneflesh,Ironflesh,Waterbreathing,Ebonyflesh,Dragonhide,--PSIJIC TELEPORT SPELLS--,Shadowstep,Blink,Teleport,Omnipresence,-Glamoril,Teleportation,Time Marches On,Force field,Blur,CHIM,
```

As I put "," behind each mod name they can be included in the list.
That has no effect as strings like "--APOCALYPSE--" will never be a hit.
It causes no harm to have these strings included.
The huge advantage is, we can easily find the sublist for each mod and modify/replace it.
